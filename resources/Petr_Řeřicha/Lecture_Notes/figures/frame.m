# regular 10x10 frame, clamped supports k=1 loaded by horizontal
# force 10 - see handwritten notes
k=[4,1,-3;1,4,-3;-3,-3,12]
f=[0;0;-10]
#r=cgs(k,f)
r=k\f
m31=2*r(1)-3*r(3)
m13=r(1)-3*r(3)
v13=(m31+m13)/10
v34=(m31+m31)/10
printf("load case homo temp in girder \n");
f=[0;-3;-6]
r=k\f
m31=2*r(1)-3*r(3)
m13=r(1)-3*r(3)
m42=2*r(2)-3*r(3)-3
v13=(m31+m13)/10
v34=(m31+m31)/10

# simple frame 8x4m, at joint 4 hinge, k=1 for girder, k=2 for legs. 
# load: t in girder alfa*t=1.
# prim unknowns phi3=r(1), psi1,3=r(2)
k=[5.5,-6;-6,15]
f=[0;6]
r=k\f
m31=2*(2*r(1)-3*r(2))
m13=2*(r(1)-3*r(2))
m24=2*(-1.5*(r(2)-2))
m34=1.5*r(1)

# HW E4 
printf("HW E4\n");
k=[4.5,3,-6;3,6,-6;6,6,-13.5]
f=[0;0;-10]
u=-k\f
M32=3*(u(1)-2*u(3)+u(2))
M35=1.5*u(3)
M12=-1.5*2*u(2)
M45=-1.5*u(3)
