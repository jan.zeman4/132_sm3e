%Here is the octave script to produce the table. The lEA, N0,N1,.. values
%must be supplied, and member (double) indices in printf() commands.
lEA=[2.27,1,1,1.414,1,1,1,1.414,1,2.27]';
N0=[0,2,-1,-1.414,1,1,0,-1.414,0,0]';
N1=[1,-1.79,0.45,0.62,-0.44,-1.34,0,0.62,0,0]';
N2=[0,0.874,-1.316,-0.62,0.44,0.44,-0.88,-0.62,0.44,1]';
D01=lEA.*N0.*N1;
D02=lEA.*N0.*N2;
d11=lEA.*N1.*N1;
d12=lEA.*N1.*N2;
d22=lEA.*N2.*N2;
all=[lEA,N0,N1,N2,D01,D02,d11,d12,d22]
sums=sum(all,1);
C=[sums(7),sums(8);sums(8),sums(9)]
psdefl=[sums(5);sums(6)]
Xs=-C\psdefl
N=N0+Xs(1).*N1+Xs(2).*N2
all=[all,N]
printf("mem&l/EA&N_0&N_1&N_2&\\Delta_{0,1}&\\Delta_{0,2}&\\delta_{1,1}&...
 \\delta_{1,2}&\\delta_{2,2}&N"); 
printf("\\\\\n 1,6");
printf("&%3.3f  ",all(1,:));
printf("\\\\\n 1,4");
printf("&%1.3f  ",all(2,:));
printf("\\\\\n 3,5");
printf("&%1.3f  ",all(3,:));
printf("\\\\\n 3,4");
printf("&%1.3f  ",all(4,:));
printf("\\\\\n 4,5");
printf("&%1.3f  ",all(5,:));
printf("\\\\\n 4,6");
printf("&%1.3f  ",all(6,:));
printf("\\\\\n 5,7");
printf("&%1.3f  ",all(7,:));
printf("\\\\\n 5,6");
printf("&%1.3f  ",all(8,:));
printf("\\\\\n 6,7");
printf("&%1.3f  ",all(9,:));
printf("\\\\\n 7,8");
printf("&%1.3f  ",all(10,:));
printf("\\\\\n \\hline \n &&&& ");
printf("&%1.3f  ",sums(5:9));
