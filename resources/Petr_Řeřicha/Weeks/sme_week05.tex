\documentclass[12pt]{article}
\usepackage{graphics,my,comb}
\usepackage{makeidx}

\graphicspath{{figures/}}

\oddsidemargin 0mm

\renewcommand{\sketch}{Sketch}
\renewcommand{\arule}{Rule}
\renewcommand\contentsname{Contents}
\renewcommand\refname{References}
\renewcommand\tablename{Table}
\usepackage{times}
\makeindex
%\flushbottom

\begin{document}
\title{ME3E course, deflections in trusses, exercise, week 5}
\author{Petr \v Re\v richa }
%\month{May}
%\year{1997}
%\date{}
\maketitle
%\tableofcontents

\topmargin -15mm
\topmargin -24mm
% the page size required by CVUT Edition center:
\textheight 250mm
\textwidth 160mm
\oddsidemargin 0mm
\footskip 10mm
\pagestyle{plain}
\textfloatsep 0pt

The examples complement section 8 of the lecture notes
'deflections in plane trusses' on the course web page. They
should substitute classes of the 5th week of the semester.
Questions concerning the stuff, address please to
'petr.rericha@fsv.cvut.cz'. HW 5 should be delivered at the same address
by the next tuesday, March, 23th.
An intermittent resume of the solution methods and skills of statically 
determinate trusses is mixed in the examples with the principal issue
of this week, deflections in trusses. Years of teaching experience
convince me it is useful.

Temperature changes are given in Kelvins (K).
\emph{All joints in truss sketches are hinges by default!} 

\section{Method of joints, force load}
Joints
1, 2 and 3 in the truss below are all hinges. Consequently, only
normal (member) forces occur in truss members, no shear forces and moments.
Two methods are used to determine member forces, the joint and the section
method. The joint method uses the joint equilibrium, in the actual
load case in the simplest truss below:
\[ 1 \uparrow : -3 - N_{1,3} {3\over 5}= 0, ~~~~\Rightarrow  N_{1,3}=
   -5 \]
\[ 1 \rightarrow : -N_{1,3} {4\over 5} - N_{2,3}=0,~~~~\Rightarrow N_{2,3}\]
Some common notation conventions should be used. 
\emph{
Every equilibrium equation should be labeled in the protocol. Member
forces are double indexed by the joint numbers they connect to.
Member forces are inserted in the respective sketch in attached boxes
as soon as they have been computed.}
Judicious application of the equilibrium equations simplifies and
accelerates the solution. Standard solution of the joint 3 equilibrium
would result in two coupled equilibrium equations. It can be avoided
as documented above when we use first the condition in which just a single
unknown member force appears. 
The required displacements are computed by the PVW:
\[ u= \sum \bar \varepsilon \delta N_{i,j} l_{i,j}= 1{4\over EA}\cdot 4=
   {16\over EA} \]
\[ v= (-1.33\cdot 4 \cdot 4 - 1.66\cdot 5\cdot 5)/EA=-63.3/EA \]
\insFigUp{truss1.pdf}{0.8\textwidth}
\\
Three simple rules are worth remembering for the solution by the mehod of
joints, \emph{all apply to members connecting at a point and are valid when
the joint is not loaded} : 
\begin{enumerate}
\item \emph{when just two members connect at the joint then 
both member forces are 0}. 
\item
 \emph{when three members connect at the joint and two of them 
lie in a line then member force in the third one is 0}
\item
 \emph{when four members connect at the joint, two of them
lie in a line and the other two are at the same angle to that line,
then the member forces the two latter ones equal each other
but for opposite signs}
\item
\emph{when four members connect at the joint, two pairs of them
lie in two lines and the joint is not loaded then each pair can be 
considered a single member and the joint may cease to be a joint.}
\end{enumerate}
The rules can be utilized/checked in the truss below. Except $N_{1,3}=0$
all other zero member forces can be determined by the first rule
without any computation.  $N_{1,4}=-N_{2,4}$ follows from the third rule
in the actual load case.   
\\
\insFigUp{truss2.pdf}{\textwidth}
\\
When the truss has more than 5-6 members it usually pays to organize
the computation in a table:
\\
\[ \begin{array}{ccrrrrr}
member&l/EA&N&\delta N&v& \delta N=N&u \\
1,2& 8&  0.5& 0.666& 2.66&  0.5&2.0 \\
1,4& 5&0.625&-0.833&-2.66&0.625&1.95\\
1,3& 3&    0&     0&    0&     &   0\\
3,4& 4&   -1&     0&    0&   -1&   4\\
2,4& 5&-0.625&-0.833&2.66&-0.625&1.95\\
2,5& 3&    0&     0&    0&    0&   0\\
4,5& 4&    0&     0&    0&    0&   0\\
\hline
&&&&                 2.66&& 9.9
\end{array}
\]
Unit $EA$ is used in the table. The two deflections computed, 2.66
and 9.9 will have to be divided by true $EA$ to obtain true deflections.
Virtual load case for $\delta u$ is the same as the actual load case $N$
and thus could be omitted. In columns $v$ and $u$
the contributions of members are collected to the virtual work, 
their sums in the last row are the two displacements wanted. Columns
$v$ and $u$ contain the expressions $\delta N_{i,j}N_{i,j}l_{i,j}/EA$
and $N_{i,j}^2 l_{i,j}/EA$ respectively. Note that for each deflection
wanted, the corresponding virtual forces column is needed.

\subsection{Thermal load}

A homogeneous temperature increase $t=20~K$ in member 1,4 
is the next actual load
case to solve for the same deflections $u$ and $v$.
\\
\insFigUp{truss2-t.pdf}{0.5\textwidth}
\\
The single member affected is $1,4$ so that only it's contribution
to the virtual work need be considered. The virtual load cases from
the previous solution can be reused, the table is not necessary:
\[ u= t \alpha l_{1,4} \delta N_{1,4}= 20 \alpha \cdot 4 \cdot -0.833=
-66.6 \alpha \]
This is the true deflection, cross-section stiffness $EA$ is
irrelevant for the thermal effects.
Note also that in the above example the reactions were not necessary
and remained unknown.

\section{Method of sections}
The truss below is loaded by temperature increase $t=20K$ in two members
$a$ and $b$ indicated in the sketch. 
Just the two member forces have to be computed
in the virtual load case since two members are subject to the thermal
load in the actual load case.
\[ v= \sum_{all members} \delta N \bar\varepsilon l = \delta N_a \alpha 
t l_a + \delta N_b \alpha t l_b  \]
The section method for determining member
forces in trusses can be demonstrated on this example. 
\\
\insFigUp{truss3-t.pdf}{\textwidth}
\\
In order to solve the virtual load case, the truss is divided
into two parts by a fictitious section shown in the sketch.
\emph{The section must cut exactly three members with yet unknown
member forces.} Then a suitable moment equilibrium condition is 
written of one of those two parts. 
\emph{The reference point of the condition
always is in the intersection of two of those cut members axes.
The condition determines then the member force in the third cut member.}
For instance  in order to determine $N_a$, the cut is indicated
by the thin curve and moment equilibrium
of part two with respect to joint $i$ is taken:
\[ II \odot i: 0.5\cdot 4 - N_a\cdot 2 -0, ~~\Rightarrow N_a=1 \]
The cut can be curved and may go trough the reference point.
In the present instance, the cut could go through joint $i$ as indicated
by the dashed thin curve. The equilibrium equation would not change.
In order to determine $N_b$ the moment equilibrium of part II (or part I)
with respect to the intersection of members $a$ and $c$ axes is taken.
Since the intersection lies in infinity in horizontal direction,
the moment equilibrum condition degenerates to vertical forces
  equilibrum condition. For part II:
\[ II \uparrow : 0.5 - N_b\cdot \cos(45)=0,~~\Rightarrow N_b= 0.707 \]
The method of sections allows to determine a certain member force
independently of other member forces.
\[ v= (1\cdot 2 +0.707\cdot 2.83)\cdot 20 \alpha=80\alpha \]



\section{Homework 5}
\insFigUp{hw5.pdf}{\textwidth}
\\
In joint numbering in task $b$ the rule 4 above is used, 
intersection of members
1,7 and 2,6 is not a joint and the same applies to 5,7 and 4,8. 
This simplifies solution, the truss perceived this way has 2 joints
and 4 members less than if those intersections were joints. Both
perceptions are possible and lead to the same member forces.
\newpage
HW5 solution:
a:\\
\insFigUp{hw5a-sol.pdf}{\textwidth}\\
\[
\begin{array}{crrrrrr}
mem&l/EA&N&\delta N_u&u&\delta N_v&v \\
1,3&4&-10&0.5&-20&0.5&-20\\
1,4&4.46&11.2&0&0&1.12&50\\
2,4&4.46&-11.2&0&0&-1.12&50\\
2,5&4&-10&0.5&-20&-0.5&20\\
3,4&2&0&-0.5&0&-0.5&0\\
4,5&2&10&-0.5&-10&0.5&10\\
3,6&2.83&-14.1&0.71&-28.3&0.71&-28.3\\
5,6&2.83&-14.1&0.71&-28.3&-0.71&28.3\\
&&&v=&-107&u=&110
\end{array}
\]
b: Zero member forces are easy to tell by the rules above!\\
\insFigUp{hw5b-sol.pdf}{\textwidth}\\
\\
LC1:\\
\[u=4.46\cdot0.56\cdot11.1\cdot2+4(10-10)\cdot0.5=55.7,~~~~v=0 \]
LC2:\\
\[u=t\alpha l_{3,4}\delta N_{u,3,4}= 20\alpha\cdot2\cdot0.5=20\alpha,~~~
v=t\alpha l_{3,4}\delta N_{v,3,4}= 20\alpha\cdot2\cdot1=40\alpha \]



\end{document}
