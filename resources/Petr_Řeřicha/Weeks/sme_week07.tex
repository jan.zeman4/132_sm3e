\documentclass[12pt]{article}
\usepackage{graphics,my,comb}
\usepackage{makeidx}

\graphicspath{{figures/}}

\oddsidemargin 0mm

\renewcommand{\sketch}{Sketch}
\renewcommand{\arule}{Rule}
\renewcommand\contentsname{Contents}
\renewcommand\refname{References}
\renewcommand\tablename{Table}
\usepackage{times}
\makeindex
%\flushbottom

\begin{document}
\title{ME3E course, force method, exercise, homework 7}
\author{Petr \v Re\v richa }
%\month{May}
%\year{1997}
%\date{}
\maketitle
%\tableofcontents

\topmargin -15mm
\topmargin -24mm
% the page size required by CVUT Edition center:
\textheight 250mm
\textwidth 160mm
\oddsidemargin 0mm
\footskip 10mm
\pagestyle{plain}
\textfloatsep 0pt

The examples complement section 3.5 - 3.6 of the lecture notes
'force and slope defl. methods' on the course web page. They
should substitute classes of the present 7th week of the semester.
Questions concerning the stuff, address please to
'petr.rericha@fsv.cvut.cz'. HW 7 should be delivered at the same address
by tuesday, April, 7th. If possible, process the homework
in a document processor (Word, Latex,..), scanned manuscripts are
acceptable, too. 

Recall that all joints are hinges by default in trusses.
Internal forces, deformations, deflections
 and other quantities in the primary structure owing to given load
are indicated by subscript $_0$. Virtual external and internal forces 
are not marked by prefix $\delta$ but indicated by subscript $_1$.
They are always the same as the actual  external and internal forces
in the primary structure loaded by the unit redundant force.
The load case $_1$ and its
quantities with subscripts $_1$ play thus two roles in the force method. 

The cross-section stiffnesses $EA=1$, $EJ=1$ are assumed in most examples
for brevity unless otherwise stated.

\section{Truss with a single redundant}
\insFigUp{force-m-truss1.pdf}{\textwidth}
\[ \begin{array}{ccrrrrr}
member&l/EA&N_0&N_1&\Delta_0& \delta &N \\
1,3& 1&  1&-0.71&-0.71& 0.5&1.56 \\
2,4& 1&-2&-0.71&1.41&0.5&-1.44\\
1,4&1.41&1.41&1&2&1.41&0.625\\
2,3&1.41&0&1&0&1.41&-0.79\\
3,4&1&-1&-0.71&0.71&0.5&-0.44\\
3,5&1 \\
4,6&1&-1&&&&-1\\
3,6&1.41&1.41&&&&1.41\\
5,6&1&-1&&&&-1\\
\hline
&&&&                 3.41&4.33
\end{array}
\]
Zero entries in the tables are sometimes omitted forthwith. 
Columns $\Delta_0$ and $\delta$ contain contributions of individual
truss members to the respective deflections. They are evaluated
just like the deflection were evaluated in week 6 exercises.
Redundant force $X=-3.41/4.33=-0.79$
Final member forces are obtained by superposition:
\[ N= N_0+XN_1\]
i.e. $column 3 +X\cdot column 4$.

\section{Deflections in statically indeterminate trusses and frames}
The issue is demostrated on the truss just solved above. Lets say we want to
compute deflections $u$ and $v$ indicated in the left sketch below.
Standard algorithm requires solution of two new
virtual load cases in the original statically indeterminate truss.

The deflection $u$ is considered first. 
The virtual load case $\delta N_{u,orig}$ 
is in the right sketch below, obviously identic with the final $N$
diagram in the right sketch above. The first variant of the expression
for $u$ below can be replaced by the second variant in which all
member forces are already known. Principle of virtual work offers
yet another variant.
Instead of the $\delta N_{u,orig}$ stress state
another arbitrary equilibrium stress state can be used as $\delta N$
provided the load in that state is a single unit pin force $\delta 1$.
Among others, the $\delta N_{u,prim}$ stress state matches
this condition (the central sketch below), making the third variant
of the expression for $u$ possible, see below. Finally, $\delta N_{u,prim}$
stress state coincides with the $N_0$ stress state of the 
above solutions so that the fourth variant is possible in which again
all member forces are already known.
\[ u=\sum \delta N_{u,orig,i} N_i (l/EA)_i=
  \sum N_i N_i (l/EA)_i=
   \sum \delta N_{u,prim,i} N_i (l/EA)_i=
  \sum N_{0,i} N_i (l/EA)_i \]
The sums run over all members of the truss which are indexed by a single
subscript $i$ for brevity in the expressions. Apparently, no new solution
for member forces is necessary for $u$, on the contrary, two expressions
are available in terms of the known member forces. The results must be the
same which is checked in the extended evaluation table below. 
This solution for $u$ is so simple because incidentally the original
load was a single pin force acting at the same place and in the same 
direction where the displacement was required. The solution for $v$
is general in this sense.
\\
\insFigUp{force-m-truss1-def.pdf}{\textwidth}
\\
Deflection $v$ is more labourious since the virtual stress state is not
available yet. The standard virtual state is in the right sketch below
but there are yet unknown member forces in it and the statically 
indeterminate truss would have to be solved to determine them. 
Another arbitrary
equilibrium stress state can be used as  $\delta N$ in which just
a single pin force is acting in the desired place and direction.
The central sketch below matches the conditions and all member forces
can be solved for by equilibrium conditions only since the truss
is statically determinate.
\[ v=\sum \delta N_{v,orig,i} N_i (l/EA)_i=
   \sum \delta N_{v,prim,i} N_i (l/EA)_i \]
\insFigUp{force-m-truss1-def-v.pdf}{\textwidth}
The arithmetics are collected in the extended table:
\[ \begin{array}{ccrrrrrrrrr}
member&l/EA&N_0&N_1&\Delta_0& \delta &N&u&u&\delta N_{v,prim}&v \\
1,3& 1&  1&-0.71&-0.71& 0.5&1.56&1.56&2.44&0& \\
2,4& 1&-2&-0.71&1.41&0.5&-1.44&2.88&2.16&1&-1.44\\
1,4&1.41&1.41&1&2&1.41&0.625i&1.25&0.55&0&\\
2,3&1.41&0&1&0&1.41&-0.79&0&0.88&0&\\
3,4&1&-1&-0.71&0.71&0.5&-0.44&0.44&0.19&0&\\
3,5&1 \\
4,6&1&-1&&&&-1&1&1&1&-1\\
3,6&1.41&1.41&&&&1.41&2.83&2.83&0&\\
5,6&1&-1&&&&-1&1&1&0&\\
\hline
&&&&                 3.41&4.33&&6.13&6.22&&-2.44
\end{array}
\]
The difference in the two $u$ values is in the wake of round off erors.

\section{Deflection in the continuous beam of HW 6}
Deflection $v$ in the center of the first span of the continous beam is 
computed. The beam has been solved in HW6.
\\
\insFigUp{force-m-cont-beam-defl.pdf}{\textwidth}
\\
The actual bending moment diagram $M$ has been drawn in HW6.
For the virtual stress state, the primary structure of that solution
is used, loaded by a
unit force conjugate to the displacemnt $v$.
Recall that support $B$ is released in that primary structure.
The integral $\int M\delta M \dd x$ is evaluated with the aid of the
table of integrals, in the right part of the sketch the decomposition
of the bending moment diagrams is indicated.
\[
v= -{1\over 3}2.15\cdot 0.715-1.25\cdot 1.07({1\over 3}+{1\over 2}
 +{1\over 6})\cdot 2.5+{1\over 3}3.125\cdot0.715\cdot 5
+2{5\over 12}\cdot 3.125\cdot 1.25\cdot2.5 
\]
\[
- {1\over 3}\cdot2.15\cdot0.715\cdot2 + {1\over 3}\cdot0.25\cdot0.715\cdot2
 - {1\over 3}\cdot 0.14\cdot 0.715(2+5)=4.69~(\cdot {1\over EJ})
\]
%octave script:
%-2.19*0.715*5/3-1.1*1.25*2.5+3.125*0.715*5/3+5*3.125*1.25*2.5/6+2*0.715*(0.5-2.19)/3 - 0.19*0.715*7/3
%ans =  4.6922
\\ \\ \\
{\bf HW 7, due April 7th}
\\
Compute deflection $v$ using the indicated primary structure:
\\
\insFigUp{hw7-cont-beam-defl.pdf}{0.5\textwidth}
\\
The deflection is solved for in the last exercise with another primary
structure so that a check is available and the labour put in both
solutions  can be compared.

\newpage
Virtual moment is very simple in this primary structure:\\
\insFigUp{hw7-cont-beam-defl-sol.pdf}{\textwidth}
\[ v= 1.25\cdot2.5\left((3.125\cdot {5\over 12}\cdot 2 
 -1.07*\left({1\over 3}+{1\over 2}+{1\over 6}\right)\right)= 
  4.7\cdot({1\over EJ})\]

\end{document}
