\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{SM3Ereview_test}[2022/03/28 Mid-term review test of course 132SME3E]

\LoadClass[11pt]{article}

%% Required packages
\RequirePackage[a4paper,margin=2cm]{geometry}
\RequirePackage[utf8]{inputenc}
\RequirePackage[numbers]{natbib}
\RequirePackage{graphicx}
\RequirePackage[colorlinks=true,linkcolor=black,citecolor=blue,urlcolor=blue]{hyperref}
\RequirePackage{fancyhdr}
\RequirePackage{pdfpages}
\RequirePackage{amsmath}
\RequirePackage{amsfonts}

%% Bibliography setup
\bibliographystyle{elsarticle-harv}

%% Graphicx setup
\graphicspath{{figures/}}

%% Other commands
\newcommand{\noitemsep}{\itemsep=0pt}

\newcounter{SessionNumber}
\newcounter{ProblemNumber}

\renewcommand{\maketitle}[1]{%
    %% Facyhdr setup
    \pagestyle{fancy}%
    \fancyhf{}%
    \chead{Structural Mechanics 3 - 132SM3E | Mid-term review test~(#1)}%
    \rfoot{\thepage}%
}

\newcommand{\Problem}[2]{%
    \refstepcounter{ProblemNumber}%
    \paragraph{Problem~\arabic{ProblemNumber}~[#1 points]}%
    #2%
}

\newcommand{\References}{%
\bibliography{liter}
}

\newcommand{\Closing}{%
\vfill
\begin{center}
    \framebox{\bfseries{Good luck!}}    
\end{center}
}

\newcommand{\ResultsTable}{%
\centerline{%
 \begin{tabular}{|l||c|c||c|}
 \hline
\bf Name and surname & \multicolumn{2}{c||}{\bf Problem} & $\boldsymbol{\Sigma}$ \\
 & \small 1 & \small 2 & \\
   \hline
\hspace{120mm} & \hspace{7mm} & \hspace{7mm} & \hspace{7mm} \\[12mm] 
\hline
\end{tabular}
}}
