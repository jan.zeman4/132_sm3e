\documentclass{SM3Eproblems}

\begin{document}
    
\maketitle{9}{Force method for statically indeterminate frame structures}

\Problem{%
\cite[p.~406, Example 9.2]{Hibbeler:2019:SA}
Using the force method, draw the shear and moment diagrams for the beam shown below. The support $B$ settles $40$~mm. Take $E = 200$~GPa and $I = 500 \cdot 10^6$~mm$^4$.
%
\begin{center}
    \def\svgwidth{.5\textwidth}
    \input{figures/09-01.pdf_tex}     
\end{center}
%
}{Recall that
%
\begin{align*}
    \delta_{ij} & = \sum_{p}
    \int_0^{L_p} \left( 
        \frac{M_i (x) M_j (x)}{EI}
        +
        \frac{N_i (x) N_j (x)}{EA}
        +
        \frac{V_i (x) V_j (x)}{GA_\mathrm{s}}
    \right) \mathrm{d} x
    \\
    \delta_{i0} & = \sum_{p}
    \int_0^{L_p} \left( 
        \frac{M_i (x) M_0 (x)}{EI}
        +
        \frac{N_i (x) N_0 (x)}{EA}
        +
        \frac{V_i (x) V_0 (x)}{GA_\mathrm{s}}
    \right) \mathrm{d} x
    \\
    & + \sum_{p}
    \int_0^{L_p} \left(
        M_i( x ) \alpha_\mathrm{T} 
        \frac{\Delta T_\mathrm{bot} - \Delta T_\mathrm{top}}{h}
        +
        N_i (x) \alpha_\mathrm{T} \Delta T_\mathrm{c}
    \right) \mathrm{d} x
    - \sum_k R_{x,ik} \widetilde{u}_k
    - \sum_l R_{z,il} \widetilde{w}_l
    - \sum_m M_{R,im} \widetilde{\varphi}_m
\end{align*}
%    
To evaluate the definite integrals, make use of the integration tables or \href
{https://www.wolframalpha.com}{online mathematics tools}.}

\Problem{%
\cite[p.~439, Problem 9-19]{Hibbeler:2019:SA}
Using the force method, determine the reactions at the supports, then draw the diagrams of internal forces for each member. Assume $A$ and $C$ are pins and the joint $B$ is fixed-connected. Take $EI$ as a constant.
%
\begin{center}
    \def\svgwidth{.55\textwidth}
    \input{figures/09-02.pdf_tex}     
\end{center}
%
}{%
Recall that
%
\begin{align*}
    \delta_{ij} & = \sum_{p}
    \int_0^{L_p} \left( 
        \frac{M_i (x) M_j (x)}{EI}
        +
        \frac{N_i (x) N_j (x)}{EA}
        +
        \frac{V_i (x) V_j (x)}{GA_\mathrm{s}}
    \right) \mathrm{d} x
    \\
    \delta_{i0} & = \sum_{p}
    \int_0^{L_p} \left( 
        \frac{M_i (x) M_0 (x)}{EI}
        +
        \frac{N_i (x) N_0 (x)}{EA}
        +
        \frac{V_i (x) V_0 (x)}{GA_\mathrm{s}}
    \right) \mathrm{d} x
    \\
    & + \sum_{p}
    \int_0^{L_p} \left(
        M_i( x ) \alpha_\mathrm{T} 
        \frac{\Delta T_\mathrm{bot} - \Delta T_\mathrm{top}}{h}
        +
        N_i (x) \alpha_\mathrm{T} \Delta T_\mathrm{c}
    \right) \mathrm{d} x
    - \sum_k R_{x,ik} \widetilde{u}_k
    - \sum_l R_{z,il} \widetilde{w}_l
    - \sum_m M_{R,im} \widetilde{\varphi}_m
\end{align*}
%    
To evaluate the definite integrals, make use of the integration tables or \href
{https://www.wolframalpha.com}{online mathematics tools}.}    

\Problem{%
\cite[p.~439, Example 9-22]{Hibbeler:2019:SA}
Using the force method, draw the distribution of internal forces resulting the vertical settlement at $A$ and non-uniform temperature change of segment $B$--$C$ whose thickness is $h = 500$~mm. Assume $A$ and $D$ are pins. Take $E = 200$~GPa, $\alpha = 1.2 \cdot 10^{-5}$~K$^{-1}$, and $I = 235 \cdot 10^6$~mm$^4$. 
%
\begin{center}
    \def\svgwidth{.525\textwidth}
    \input{figures/09-03.pdf_tex}     
\end{center}
%
}{%
Recall that
%
\begin{align*}
    \delta_{ij} & = \sum_{p}
    \int_0^{L_p} \left( 
        \frac{M_i (x) M_j (x)}{EI}
        +
        \frac{N_i (x) N_j (x)}{EA}
        +
        \frac{V_i (x) V_j (x)}{GA_\mathrm{s}}
    \right) \mathrm{d} x
    \\
    \delta_{i0} & = \sum_{p}
    \int_0^{L_p} \left( 
        \frac{M_i (x) M_0 (x)}{EI}
        +
        \frac{N_i (x) N_0 (x)}{EA}
        +
        \frac{V_i (x) V_0 (x)}{GA_\mathrm{s}}
    \right) \mathrm{d} x
    \\
    & + \sum_{p}
    \int_0^{L_p} \left(
        M_i( x ) \alpha_\mathrm{T} 
        \frac{\Delta T_\mathrm{bot} - \Delta T_\mathrm{top}}{h}
        +
        N_i (x) \alpha_\mathrm{T} \Delta T_\mathrm{c}
    \right) \mathrm{d} x
    - \sum_k R_{x,ik} \widetilde{u}_k
    - \sum_l R_{z,il} \widetilde{w}_l
    - \sum_m M_{R,im} \widetilde{\varphi}_m
\end{align*}
%    
To evaluate the definite integrals, make use of the integration tables or \href
{https://www.wolframalpha.com}{online mathematics tools}.}    

\SelfStudy{%

\begin{itemize}
    \item \cite[Sections 9.1, 9.2, 9.3, 9.4, and 9.5]{Hibbeler:2019:SA}
\end{itemize}

}

\References

\Request

\end{document}